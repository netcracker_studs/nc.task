package edu.netcracker.jobdealer.service;

import edu.netcracker.jobdealer.entity.Applicant;
import edu.netcracker.jobdealer.entity.Submission;
import edu.netcracker.jobdealer.entity.Task;

import java.util.List;

public interface SubmissionService {

    public Submission commitSubmission(Task task, String filename, Applicant user);

    public List<Applicant> showSubmitters(Task task);

}
