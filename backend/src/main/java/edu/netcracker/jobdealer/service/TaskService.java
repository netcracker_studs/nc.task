package edu.netcracker.jobdealer.service;

import edu.netcracker.jobdealer.entity.Task;
import edu.netcracker.jobdealer.entity.Vacancy;
import edu.netcracker.jobdealer.exceptions.TaskNotFoundException;

public interface TaskService {
    public Task createTask(Vacancy vacancy, String name, String description);

    public Task updateTaskName(Task task, String newName);

    public Task updateTaskDescription(Task task, String newDescription);

    public Task getTaskByVacancy(Vacancy vacancy) throws TaskNotFoundException;

    public Task getTaskById(int taskId) throws TaskNotFoundException;
}
