package edu.netcracker.jobdealer.service;

import edu.netcracker.jobdealer.dto.CompanyDto;

import java.util.List;

public interface CompanyService {

    List<CompanyDto> getAllCompanies();

    CompanyDto getCompanyById(Integer id);
    
}
