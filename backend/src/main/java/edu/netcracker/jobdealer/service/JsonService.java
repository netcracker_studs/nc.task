package edu.netcracker.jobdealer.service;

public interface JsonService {
    String toJson(Object o);
}