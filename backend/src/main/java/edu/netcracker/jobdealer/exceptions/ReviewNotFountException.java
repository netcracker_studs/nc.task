package edu.netcracker.jobdealer.exceptions;

public class ReviewNotFountException extends RuntimeException {
    public ReviewNotFountException(String msg) {
        super(msg);
    }
}
