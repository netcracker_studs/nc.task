package edu.netcracker.jobdealer.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;


@Data
@Entity
@Table(name = "resume", schema = "public", catalog = "netcracker")
public class Resume {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column(name = "firstName")
    private String first_name;
    @Basic
    @Column(name = "secondName")
    private String second_name;
    @Basic
    @Column(name = "middleName")
    private String middle_name;
    @Basic
    @Column(name = "vacancy")
    private String vacancy;
    @Basic
    @Column(name = "money")
    private Integer money;
    @Basic
    @Column(name = "avatarUrl")
    private String avatarUrl;
    @Basic
    @Column(name = "about")
    private String about;
    
    @ManyToOne
    @JoinColumn(name = "ownedResumes", referencedColumnName = "id")
    private Applicant owner;
    @OneToMany(mappedBy = "owner")
    List<SkillToOwner> skills;

    protected Resume() {
    }

    public Resume(Applicant applicant) {
        this.owner = applicant;
    }
}
